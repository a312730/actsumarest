const express = require('express');
const app = express();

app.get('/result/:n1/:n2', (request, response)=>{
  var n1 = parseInt(request.params.n1);
  var n2 = parseInt(request.params.n2);
  response.send(`the result is ${n1+n2}`);
});

app.post('/result/:n1/:n2', (request, response)=>{
  var n1 = parseInt(request.params.n1);
  var n2 = parseInt(request.params.n2);
  response.send(`the result is ${n1*n2}`);
});

app.put('/result/:n1/:n2', (request, response)=>{
  var n1 = parseInt(request.params.n1);
  var n2 = parseInt(request.params.n2);
  response.send(`the result is ${n1/n2}`);
});

app.patch('/result/:n1/:n2', (request, response)=>{
  var n1 = parseInt(request.params.n1);
  var n2 = parseInt(request.params.n2);
  response.send(`the result is ${n1**n2}`);
});

app.delete('/result/:n1/:n2', (request, response)=>{
  var n1 = parseInt(request.params.n1);
  var n2 = parseInt(request.params.n2);
  response.send(`the result is ${n1-n2}`);
});


app.listen(4000, ()=>{
  console.log("When the imposter is sus")
});
